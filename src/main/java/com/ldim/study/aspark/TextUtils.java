package com.ldim.study.aspark;

import java.io.FileOutputStream;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by ldim on 27.05.2017.
 */
public class TextUtils {
    private static final String[] WORDS = "i,yesterday,to,now,she,oh,believe,away,a,in,play,suddenly,don't,hide,long,need,love,something,why,easy".split(",");

    public static String twoWordPhrase() {
        return ThreadLocalRandom.current().ints(2, 0, WORDS.length).mapToObj(i -> WORDS[i]).collect(Collectors.joining(" "));
    }

    public static List<String> phrases(int count) {
        return IntStream.range(0, count).mapToObj(i -> twoWordPhrase()).collect(Collectors.toList());
    }

    public static void createBigFile(long sizeGb) {
        final long inBytes = 1024 * 1024 * 1024 * sizeGb;
        List<String> phrases = TextUtils.phrases(300);
        try (FileOutputStream fos = new FileOutputStream("big_data_" + sizeGb)) {
            for (long count = 0; count < inBytes; ) {
                String ln = new StringBuilder(2).append(ThreadLocalRandom.current().ints(50, 0, phrases.size())
                        .mapToObj(phrases::get).collect(Collectors.joining("|"))).append("|\n\r").toString();
                fos.write(ln.getBytes());
                count += ln.getBytes().length;
                System.out.println(count);
            }
            fos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Iterator<String> getWords(String text) {
        List<String> words = new ArrayList<>();
        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(text);
        int lastIndex = breakIterator.first();
        while (BreakIterator.DONE != lastIndex) {
            int firstIndex = lastIndex;
            lastIndex = breakIterator.next();
            if (lastIndex != BreakIterator.DONE && Character.isLetterOrDigit(text.charAt(firstIndex))) {
                words.add(text.substring(firstIndex, lastIndex));
            }
        }
        return words.iterator();
    }

    public static Iterator<String> getPhrases(String text) {
        return Arrays.asList(text.split("\\|")).iterator();
    }
}
