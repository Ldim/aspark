package com.ldim.study.aspark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ldim on 28.05.2017.
 */
public class PopularWordsCountTest {
    private JavaSparkContext sc;

    @Before
    public void init() {
        SparkConf conf = new SparkConf();
        conf.setAppName("SparkWordCountTest");
        conf.setMaster("local[*]");
        sc = new JavaSparkContext(conf);
    }

    @Test
    public void testTopX() throws Exception {
        List<String> list = Arrays.asList("Java the java the java, the", "Scala the Groovy the groovy", "pascal kotlin");
        JavaRDD<String> rdd = sc.parallelize(list);
        SparkTextProcessor sp = new SparkTextProcessor(rdd, TextUtils::getWords);
        System.out.println(sp.wordsCount());
        Assert.assertEquals("java", sp.top(3).get(1));
    }

    @After
    public void end() {
        sc.close();
    }
}
