package com.ldim.study.aspark;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import scala.Tuple2;

import java.util.List;

/**
 * Created by ldim on 28.05.2017.
 */
public class SparkTextProcessor {
    private final JavaPairRDD<String, Integer> counts;

    public SparkTextProcessor(JavaRDD<String> lines, FlatMapFunction<String, String> fn) {
        counts = lines.map(String::toLowerCase)
                .flatMap(fn)
                .mapToPair(s -> new Tuple2<>(s, 1))
                .reduceByKey(Integer::sum);
    }

    public List<Tuple2<String, Integer>> wordsCount() {
        return counts.collect();
    }

    public List<String> top(int num) {
        return counts.mapToPair(Tuple2::swap)
                .sortByKey(false)
                .map(Tuple2::_2).take(num);
    }
}
