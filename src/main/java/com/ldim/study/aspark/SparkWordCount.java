package com.ldim.study.aspark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

public final class SparkWordCount {

    public static void main(String[] args) throws Exception {

        if (args.length < 1) {
            System.err.println("Usage: SparkWordCount <file> or SparkWordCount -big <sizeGb>");
            System.exit(1);
        }

        if (args.length > 1) {
            TextUtils.createBigFile(Integer.parseInt(args[1]));
        } else {
            SparkConf conf = new SparkConf();
            conf.setAppName("SparkWordCount");
            conf.setMaster("local[10]");
            try (JavaSparkContext sc = new JavaSparkContext(conf);) {
                SparkTextProcessor stp = new SparkTextProcessor(sc.textFile(args[0]), TextUtils::getPhrases);
                for (Tuple2<?, ?> tuple : stp.wordsCount()) {
                    System.out.println("Count: " + tuple._1() + " " + tuple._2());
                }
                System.out.println("Top 50: " + stp.top(50));
                sc.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
